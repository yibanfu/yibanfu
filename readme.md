#1 一板斧监控


![一板斧监控Logo](http://git.oschina.net/yibanfu/yibanfu/raw/master/images/yibanfu-logo.png)



-  **一板斧**监控平台，通过在Linux/Windows下部署简单的脚本实现对服务器运行状态的监控

-  支持通过微信管理和监控服务器状态。

**官方网站**：[一板斧监控官方网站 http://yibanfu.com](http://yibanfu.com)


#2 系统支持

Linux类操作系统支持CentOS / Ubuntu / Fedora / Redhat / Debian / SUSE 等，

Windows系统支持Windows 2003/2008/7/8等。

详细请参考 [一板斧客户端操作系统支持清单](https://git.oschina.net/yibanfu/yibanfu/wikis/%E4%B8%80%E6%9D%BF%E6%96%A7%E5%AE%A2%E6%88%B7%E7%AB%AF%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E6%94%AF%E6%8C%81%E6%B8%85%E5%8D%95)

#3 下载地址

**客户端下载**：[一板斧监控客户端 - git@oschina https://git.oschina.net/yibanfu/yibanfu](https://git.oschina.net/yibanfu/yibanfu) 



#4 产品特性


##4.1 部署轻松
- 注册一个 **一板斧** 帐号
- 部署一个 **Python/VBS** 脚本
- 创建一个计划任务


##4.2 使用简单
- 微信查看服务运行状态
- 微信接收服务器异常告警
- Web浏览服务器参数详情


##4.3 也很安全
- 全程 **HTTPS** 通讯，加密通信
- 支持 **AES** 加密，增强数据安全 (需要安装加密库[PyCrypto](http://www.python.org/pypi/pycrypto/))
- 纯Python打造，源代码透明，既没有任何隐藏，也不收集敏感信息；实在是居家旅行、网站运维必备工具啊！


#5 部署指导
--------

基本步骤如下：

- 注册 **一板斧** 帐号
 - 访问[ **一板斧官方网站** http://yibanfu.com](http://yibanfu.com)，使用E-mail注册帐号。
 - 关注微信公众账号 **一板斧** ，绑定注册帐号。
- 下载客户端脚本
- 修改脚本中参数
- 测试脚本
- 创建系统计划任务


##5.1 Linux

- [轻量级客户端linux版安装手册](https://git.oschina.net/yibanfu/yibanfu/wikis/%E8%BD%BB%E9%87%8F%E7%BA%A7%E5%AE%A2%E6%88%B7%E7%AB%AFLinux%E7%89%88%E5%AE%89%E8%A3%85%E6%89%8B%E5%86%8C) （推荐优先使用）
- [数据安全增强型客户端Linux版安装手册](https://git.oschina.net/yibanfu/yibanfu/wikis/%E6%95%B0%E6%8D%AE%E5%AE%89%E5%85%A8%E5%A2%9E%E5%BC%BA%E5%9E%8B%E5%AE%A2%E6%88%B7%E7%AB%AFLinux%E7%89%88%E5%AE%89%E8%A3%85%E6%89%8B%E5%86%8C) (推荐用于对通讯安全有非常高要求的服务器)

##5.2 Windows

- [轻量级客户端Windows版安装手册](https://git.oschina.net/yibanfu/yibanfu/wikis/%E8%BD%BB%E9%87%8F%E7%BA%A7%E5%AE%A2%E6%88%B7%E7%AB%AFWindows%E7%89%88%E5%AE%89%E8%A3%85%E6%89%8B%E5%86%8C)（推荐优先使用）


#6 运行监控
您有两种方式可以查看已经部署客户端监控脚本的服务器：   **Web** 和 **微信** 

- 登录[一板斧官方网站http://yibanfu.com/login](http://yibanfu.com/login) Web后台查看。（很大气！）

![yibanfu-server-admin-main.png](http://git.oschina.net/yibanfu/yibanfu/raw/master/images/server/yibanfu-server-admin-main.png)
- 关注一板斧微信公众帐号，通过微信查看系统运行状态。（很easy!）

![yibanfu-weixin-details.png](http://git.oschina.net/yibanfu/yibanfu/raw/master/images/weixin/yibanfu-weixin-details.png)


#7 FAQ
 
##7.1 运行报错
 
-  如果您希望增强数据安全，在HTTPS的基础上再使用AES加密数据字段，或者系统版本较低（Fedora, CentOS 5.x, 6.x, Ubuntu10.04以下）， 请参考详细安装指导手册：[yibanfu-setup-linux.doc](https://git.oschina.net/yibanfu/yibanfu/raw/master/client/linux/yibanfu-setup-linux.doc)

##7.2 系统不支持或运行报错？
我们非常重视您的反馈！

如果您在安装和部署过程中遇到任何问题，请反馈给一板斧官方人员或一板斧客户QQ群：162616448。

您的任何反馈都是我们改进的需求和源动力！

![weixin/yibanfu-weixin-gongzhong.jpg](http://git.oschina.net/yibanfu/yibanfu/raw/master/images/weixin/yibanfu-weixin-gongzhong.jpg)